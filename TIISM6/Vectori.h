void Citire(int[], int&);
void Afisare(int[], int);
void AfisareInvers(int[], int);

// un numae e shmeker daca
// eliminand prima si ultima cifra
// suma divizorilor este numar prim
int CateNumereShmekere(int[], int);
bool Shmeker(int);

int Suma(int[], int);
int ProdusPare(int[], int);
int MinimPozitive(int[], int);
int MaximImpare(int[], int);
int CatePrime(int[], int);
int MediaAritmeticaNegative(int[], int);

