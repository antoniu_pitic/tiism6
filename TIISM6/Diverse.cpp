#include "Diverse.h"

int Invers(int n){
	int m = 0;
	for (int t = n; t; t /= 10){
		m = m * 10 + t % 10;
	}
	return m;
}

bool Prim(int n){
	for (int d = 2; d <= n / 2; d++){
		if (n%d == 0){
			return false;
		}
	}
	return true;
}

int SumaDivizori(int n){
	int S = 0;
	for (int d = 1; d <= n; d++){
		if (n%d == 0){
			S += d;
		}
	}
	return S;
}