#include "Serii.h"
#include <iostream>
using namespace std;

// 1 21 321 4321 .... n..1
void Serie1(int n){
	for (int k = 1; k <= n; k++){
		Descrescator(k);
		cout << " ";
	}
	cout << endl;
}


void Descrescator(int n){
	for (int i = n; i >= 1; i--){
		cout << i;
	}
}

/*
   1
  121
 12321
1234321
 12321
  121
   1
*/

void RombDinNumere(int n){
	int k;
	for (k = 1; k <= n; k++){
		NisteSpatii(n-k);
		Crescator(k);
		Descrescator(k - 1);
		cout << endl;
	}
	k--;
	while(--k){
		NisteSpatii(n - k);
		Crescator(k);
		Descrescator(k - 1);
		cout << endl;
	}
}

void NisteSpatii(int n){
	for (int i = 1; i <= n; i++){
		cout << " ";
	}
}

void Crescator(int n){
	for (int i = 1; i <= n; i++){
		cout << i;
	}
}