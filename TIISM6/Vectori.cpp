#include <iostream>
using namespace std;
#include "Diverse.h"
#include "Vectori.h"

void Citire(int a[], int &n){
	cin >> n;
	for (int i = 0; i < n; i++){
		cin >> a[i];
	}
}

void Afisare(int v[], int L){
	for (int i = 0; i < L; i++){
		cout << v[i] << " ";
	}
	cout << endl;
}

void AfisareInvers(int v[], int L){
	for (int i = L-1; i >=0; i--){
		cout << v[i] << " ";
	}
	cout << endl;
}


int Suma(int a[], int n){
	int S = 0;
	for (int i = 0; i < n; i++){
		S += a[i];
	}
	return S;
}



int CateNumereShmekere(int a[], int n){
	int ct = 0;
	for (int i = 0; i < n; i++){
		if (Shmeker(a[i])){
			ct++;
		}
	}
	return ct;
}

// un numar e shmeker daca
// eliminand prima si ultima cifra
// suma divizorilor este numar prim

bool Shmeker(int n){
	n /= 10;
	n = Invers(n);
	n /= 10;
	n = Invers(n);

	return Prim(SumaDivizori(n));
}